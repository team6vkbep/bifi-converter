import bifi.importmodel.OldAddress;
import org.junit.Assert;
import org.junit.Test;

public class OldAddressTest {
    @Test
    public void OldAddressConstructorTest(){
        // Arrange
        final String street = "street";
        final String addressType = "addressType";
        final String houseNumber = "houseNumber";
        final String postalCode = "postalCode";
        final String city = "city";
        final String bic = "bic";

        // Act
        OldAddress oldAddress = new OldAddress(street, addressType, houseNumber, postalCode, city, bic);

        // Assert
        Assert.assertNotNull(oldAddress);
        Assert.assertTrue(oldAddress.getStreet().equals(street));
        Assert.assertTrue(oldAddress.getAddressType().equals(addressType));
        Assert.assertTrue(oldAddress.getHouseNumber().equals(houseNumber));
        Assert.assertTrue(oldAddress.getPostalCode().equals(postalCode));
        Assert.assertTrue(oldAddress.getCity().equals(city));
        Assert.assertTrue(oldAddress.getBic().equals(bic));
    }
}
