import bifi.exportmodel.AddressTypeEnum;
import org.junit.Test;

public class AddressTypeEnumTest {
    @Test
    public void addressTypeInvoiceTest(){
        // Arrange
        AddressTypeEnum addressTypeInvoice;

        // Act
        addressTypeInvoice  = AddressTypeEnum.ADDRESS_TYPE_INVOICE;

        // Assert
        addressTypeInvoice.toString().equals("i");
    }

    @Test
    public void addressTypeDeliveryTest(){
        // Arrange
        AddressTypeEnum addressTypeInvoice;

        // Act
        addressTypeInvoice  = AddressTypeEnum.ADDRESS_TYPE_DELIVERY;

        // Assert
        addressTypeInvoice.toString().equals("d");
    }

    @Test
    public void addressTypeMailingTest(){
        // Arrange
        AddressTypeEnum addressTypeInvoice;

        // Act
        addressTypeInvoice  = AddressTypeEnum.ADDRESS_TYPE_MAILING;

        // Assert
        addressTypeInvoice.toString().equals("m");
    }
}
