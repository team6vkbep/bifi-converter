import bifi.importmodel.OldPerson;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class OldPersonTest {
    private String firstName;
    private String insertion;
    private String lastName;

    @Before
    public void initiateOldPerson(){
        // Arrange
        firstName = "testFirstName";
        insertion = "testInsertion";
        lastName = "testLastName";
    }

    @Test
    public void OldPersonConstructorTest(){
        // Arrange
        final String testGender = "testGender";

        // Act
        OldPerson oldPerson = new OldPerson(firstName, insertion, lastName, testGender);

        // Assert
        Assert.assertNotNull(oldPerson);
        Assert.assertTrue(oldPerson.getFirstName().equals(firstName));
        Assert.assertTrue(oldPerson.getInsertion().equals(insertion));
        Assert.assertTrue(oldPerson.getLastName().equals(lastName));
        Assert.assertTrue(oldPerson.getSalutation().equals("Dhr."));
    }

    @Test
    public void OldPersonSalutationMaleTest(){
        // Arrange
        final String maleGender = "m";

        // Act
        OldPerson oldPerson = new OldPerson(firstName, insertion, lastName, maleGender);

        // Assert
        Assert.assertNotNull(oldPerson);
        Assert.assertTrue(oldPerson.getSalutation().equals("Dhr."));
    }

    @Test
    public void OldPersonSalutationFemaleTest(){
        // Arrange
        final String femaleGender = "f";

        // Act
        OldPerson oldPerson = new OldPerson(firstName, insertion, lastName, femaleGender);

        // Assert
        Assert.assertNotNull(oldPerson);
        Assert.assertTrue(oldPerson.getSalutation().equals("Mvr."));
    }
}
