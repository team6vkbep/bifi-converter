import bifi.importmodel.OldAddress;
import bifi.importmodel.OldCustomer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class OldCustomerTest {
    private OldAddress oldAddress;

    private String companyName;
    private String vat;
    private String iban;
    private OldCustomer oldCustomer;

    @Before
    public void initializeObjects(){
        // Arrange
        final String street = "testStreet";
        final String addressType = "testAddressType";
        final String houseNumber = "testHouseNumber";
        final String postalCode = "testPostalCode";
        final String city = "testCity";
        final String bic = "testBic";

        oldAddress = new OldAddress(street, addressType, houseNumber, postalCode, city, bic);

        companyName = "testCompanyName";
        vat = "testVat";
        iban = "testIban";

        oldCustomer = new OldCustomer(companyName, vat, iban, oldAddress);
    }

    @Test
    public void OldCustomerTest(){
        Assert.assertNotNull(oldCustomer);
        Assert.assertTrue(oldCustomer.getCompanyName().equals(companyName));
        Assert.assertTrue(oldCustomer.getVat().equals(vat));
        Assert.assertTrue(oldCustomer.getIban().equals(iban));
        Assert.assertTrue(oldCustomer.getAddress().equals(oldAddress));
    }
}
