import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import bifi.converter.JsonConverter;
import bifi.exportmodel.*;

public class JsonConverterTest {
    private static CompleteInvoice correctCompleteInvoice = null;

    @BeforeClass
    public static void GetCorrectCompleteInvoice(){
        // Arrange
        String correctJsonString = Helper.getCorrectJsonString();
        JsonConverter jsonConverter = new JsonConverter();

        // Act
        correctCompleteInvoice = jsonConverter.convertJsonStringToCompleteInvoiceExport(correctJsonString);
    }

    @Test
    public void JsonConverter_CompleteInvoiceTest(){
        // Assert
        Assert.assertNotNull(correctCompleteInvoice);
        Assert.assertNotNull(correctCompleteInvoice.getCompanies());
        Assert.assertTrue(correctCompleteInvoice.getCompanies().size() >= 1);
    }

    @Test
    public void JsonConverter_CompanyTest(){
        // Act
        Company correctCompany = correctCompleteInvoice.getCompanies().get(0);

        // Assert
        Assert.assertNotNull(correctCompany );
        Assert.assertNotNull(correctCompany.getName() );
        Assert.assertNotNull(correctCompany.getBtwNumber() );
        Assert.assertNotNull(correctCompany.getIban() );
        Assert.assertNotNull(correctCompany.getAddress() );
        Assert.assertNotNull(correctCompany.getCustomers() );
        Assert.assertTrue(correctCompany.getCustomers().size() >= 1 );
    }

    @Test
    public void JsonConverter_CompanyAddressTest(){
        // Act
        Address correctCompanyAddress = correctCompleteInvoice.getCompanies().get(0).getAddress();

        // Assert
        correctAddressTest(correctCompanyAddress);
    }

    @Test
    public void JsonConverter_CustomerTest(){
        // Act
        Customer correctCustomer = correctCompleteInvoice.getCompanies().get(0).getCustomers().get(0);

        // Assert
        Assert.assertNotNull(correctCustomer);
        Assert.assertNotNull(correctCustomer.getSalutation());
        Assert.assertNotNull(correctCustomer.getFirstName());
        Assert.assertNotNull(correctCustomer.getInsertion());
        Assert.assertNotNull(correctCustomer.getLastName());
        Assert.assertNotNull(correctCustomer.getBtwCode());
        Assert.assertNotNull(correctCustomer.getIban());
        Assert.assertNotNull(correctCustomer.getAddress());
        Assert.assertNotNull(correctCustomer.getInvoices());
        Assert.assertTrue(correctCustomer.getInvoices().size() >= 1);
    }

    @Test
    public void JsonConverter_CustomerAddressTest(){
        // Act
        Address correctCustomerAddress = correctCompleteInvoice.getCompanies().get(0).getCustomers().get(0).getAddress();

        // Assert
        correctAddressTest(correctCustomerAddress);
    }

    @Test
    public void JsonConverter_InvoiceTest(){
        // Act
        Invoice correctInvoice = correctCompleteInvoice.getCompanies().get(0).getCustomers().get(0).getInvoices().get(0);

        // Assert
        Assert.assertNotNull(correctInvoice);
        Assert.assertNotNull(correctInvoice.getInvoiceDate());
        Assert.assertNotNull(correctInvoice.getInvoiceNumber());
        Assert.assertNotNull(correctInvoice.getInvoiceLines());
        Assert.assertTrue(correctInvoice.getInvoiceLines().size() >= 1);
    }

    @Test
    public void JsonConverter_InvoiceLineTest(){
        // Act
        InvoiceLine correctInvoiceLine = correctCompleteInvoice.getCompanies().get(0).getCustomers().get(0).getInvoices().get(0).getInvoiceLines().get(0);

        // Assert
        Assert.assertNotNull(correctInvoiceLine);
        Assert.assertNotNull(correctInvoiceLine.getInvoiceTime());
        Assert.assertNotNull(correctInvoiceLine.getProductDescription());
        Assert.assertNotNull(correctInvoiceLine.getAmount());
        Assert.assertNotNull(correctInvoiceLine.getPriceWithoutBtw());
        Assert.assertNotNull(correctInvoiceLine.getBtwType());
        Assert.assertNotNull(correctInvoiceLine.getUnit());
    }


    private void correctAddressTest(Address correctAddress){
        Assert.assertNotNull(correctAddress );
        Assert.assertNotNull(correctAddress.getStreet() );
        Assert.assertNotNull(correctAddress.getAddressType() );
        Assert.assertNotNull(correctAddress.getHouseNumber() );
        Assert.assertNotNull(correctAddress.getPostalCode() );
        Assert.assertNotNull(correctAddress.getCity() );
        Assert.assertNotNull(correctAddress.getBic() );
    }

    @Test
    public void JsonConverterTest_IncorrectJsonString() {
        // Arrange
        String incorrectJsonString = Helper.getIncorrectJsonString();
        JsonConverter jsonConverter = new JsonConverter();

        // Act
        CompleteInvoice completeInvoice = jsonConverter.convertJsonStringToCompleteInvoiceExport(incorrectJsonString);

        // Assert
        Assert.assertTrue(completeInvoice == null);
    }

    @Test
    public void JsonConverterTest_EmptyJsonString() {
        // Arrange
        String emptyJsonString = "";
        JsonConverter jsonConverter = new JsonConverter();

        // Act
        CompleteInvoice completeInvoice = jsonConverter.convertJsonStringToCompleteInvoiceExport(emptyJsonString);

        // Assert
        Assert.assertTrue(completeInvoice == null);
    }
}
