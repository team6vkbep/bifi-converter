public class Helper {

    public static String getCorrectJsonString(){
        return generateCorrectJsonString();
    }

    public static String getIncorrectJsonString(){
        return generateIncorrectJsonString();
    }

    private static String generateCorrectJsonString(){
        String s = "{\n" +
                "  \"invoices\": [\n" +
                "    {\n" +
                "      \"date\": \"25-05-2018 60:02:12\",\n" +
                "      \"note\": \"Helloworld\",\n" +
                "      \"invoiceNumber\": \"2\",\n" +
                "      \"oldCustomer\": {\n" +
                "        \"oldAddress\": {\n" +
                "          \"city\": \"Diagon\",\n" +
                "          \"street\": \"Wegisweg\",\n" +
                "          \"addressType\": \"F\",\n" +
                "          \"postalCode\": \"1234AA\",\n" +
                "          \"houseNumber\": \"123\",\n" +
                "          \"bic\": \"0\"\n" +
                "        },\n" +
                "        \"companyName\": \"Coca Cola\",\n" +
                "        \"iban\": \"INGB 1230\",\n" +
                "        \"vat\": \"EUVAT123\",\n" +
                "        \"legalForm\": \"two\",\n" +
                "        \"bic\": \"BIK 1234\",\n" +
                "        \"giro\": \"GIRO 1234\"\n" +
                "      },\n" +
                "      \"oldInvoiceLines\": [\n" +
                "        {\n" +
                "          \"id\": \"5b0e6437afc83e169fc6749b\",\n" +
                "          \"productId\": 0,\n" +
                "          \"productName\": \"Bike\",\n" +
                "          \"quantity\": 2.0,\n" +
                "          \"totalPrice\": 2.0,\n" +
                "          \"btwCode\": \"high\",\n" +
                "          \"unit\": \"pce\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"oldPerson\": {\n" +
                "        \"firstName\": \"Jos\",\n" +
                "        \"lastName\": \"Bruine\",\n" +
                "        \"gender\": \"m\",\n" +
                "        \"insertion\": \"de\",\n" +
                "        \"telephone\": \"61543452\",\n" +
                "        \"fax\": \"two\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"date\": \"25-05-2018 60:02:12\",\n" +
                "      \"note\": \"Helloworld\",\n" +
                "      \"invoiceNumber\": \"2\",\n" +
                "      \"oldCustomer\": {\n" +
                "        \"oldAddress\": {\n" +
                "          \"city\": \"Diagon\",\n" +
                "          \"street\": \"Wegisweg\",\n" +
                "          \"addressType\": \"F\",\n" +
                "          \"postalCode\": \"1234AA\",\n" +
                "          \"houseNumber\": \"123\",\n" +
                "          \"bic\": \"0\"\n" +
                "        },\n" +
                "        \"companyName\": \"Coca Cola\",\n" +
                "        \"iban\": \"INGB 1230\",\n" +
                "        \"vat\": \"EUVAT123\",\n" +
                "        \"legalForm\": \"two\",\n" +
                "        \"bic\": \"BIK 1234\",\n" +
                "        \"giro\": \"GIRO 1234\"\n" +
                "      },\n" +
                "      \"oldInvoiceLines\": [\n" +
                "        {\n" +
                "          \"id\": \"5b0e6437afc83e169fc6749b\",\n" +
                "          \"productId\": 0,\n" +
                "          \"productName\": \"Bike\",\n" +
                "          \"quantity\": 2.0,\n" +
                "          \"totalPrice\": 2.0,\n" +
                "          \"btwCode\": \"high\",\n" +
                "          \"unit\": \"pce\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"oldPerson\": {\n" +
                "        \"firstName\": \"Jos\",\n" +
                "        \"lastName\": \"Bruine\",\n" +
                "        \"gender\": \"m\",\n" +
                "        \"insertion\": \"de\",\n" +
                "        \"telephone\": \"61543452\",\n" +
                "        \"fax\": \"two\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"date\": \"25-05-2018 60:02:12\",\n" +
                "      \"note\": \"Helloworld\",\n" +
                "      \"invoiceNumber\": \"2\",\n" +
                "      \"oldCustomer\": {\n" +
                "        \"oldAddress\": {\n" +
                "          \"city\": \"Diagon\",\n" +
                "          \"street\": \"Wegisweg\",\n" +
                "          \"addressType\": \"F\",\n" +
                "          \"postalCode\": \"1234AA\",\n" +
                "          \"houseNumber\": \"123\",\n" +
                "          \"bic\": \"0\"\n" +
                "        },\n" +
                "        \"companyName\": \"Coca Cola\",\n" +
                "        \"iban\": \"INGB 1230\",\n" +
                "        \"vat\": \"EUVAT123\",\n" +
                "        \"legalForm\": \"two\",\n" +
                "        \"bic\": \"BIK 1234\",\n" +
                "        \"giro\": \"GIRO 1234\"\n" +
                "      },\n" +
                "      \"oldInvoiceLines\": [\n" +
                "        {\n" +
                "          \"id\": \"5b0e6437afc83e169fc6749b\",\n" +
                "          \"productId\": 0,\n" +
                "          \"productName\": \"Bike\",\n" +
                "          \"quantity\": 2.0,\n" +
                "          \"totalPrice\": 2.0,\n" +
                "          \"btwCode\": \"high\",\n" +
                "          \"unit\": \"pce\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"oldPerson\": {\n" +
                "        \"firstName\": \"Jos\",\n" +
                "        \"lastName\": \"Bruine\",\n" +
                "        \"gender\": \"m\",\n" +
                "        \"insertion\": \"de\",\n" +
                "        \"telephone\": \"61543452\",\n" +
                "        \"fax\": \"two\"\n" +
                "      }\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        return s;
    }

    private static String generateIncorrectJsonString(){
        String incorrectJsonString = "{\n" +
                "    \"glossary\": {\n" +
                "        \"title\": \"example glossary\",\n" +
                "\t\t\"GlossDiv\": {\n" +
                "            \"title\": \"S\",\n" +
                "\t\t\t\"GlossList\": {\n" +
                "                \"GlossEntry\": {\n" +
                "                    \"ID\": \"SGML\",\n" +
                "\t\t\t\t\t\"SortAs\": \"SGML\",\n" +
                "\t\t\t\t\t\"GlossTerm\": \"Standard Generalized Markup Language\",\n" +
                "\t\t\t\t\t\"Acronym\": \"SGML\",\n" +
                "\t\t\t\t\t\"Abbrev\": \"ISO 8879:1986\",\n" +
                "\t\t\t\t\t\"GlossDef\": {\n" +
                "                        \"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",\n" +
                "\t\t\t\t\t\t\"GlossSeeAlso\": [\"GML\", \"XML\"]\n" +
                "                    },\n" +
                "\t\t\t\t\t\"GlossSee\": \"markup\"\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "}";

        return incorrectJsonString;
    }
}
