import bifi.importmodel.OldInvoiceLine;
import org.junit.Assert;
import org.junit.Test;

public class OldInvoiceLineTest {
    @Test
    public void OldInvoiceLineConstructorTest(){
        // Arrange
        final String productName = "testProductName";
        final double quantity = 1.5;
        final double totalPrice = 10.0;
        final String btwCode = "testBtwCode";
        final String unit = "testUnit";

        // Act
        OldInvoiceLine oldInvoiceLine = new OldInvoiceLine(productName, quantity, totalPrice, btwCode, unit);

        // Assert
        Assert.assertNotNull(oldInvoiceLine);
        Assert.assertTrue(oldInvoiceLine.getProductName().equals(productName));
        Assert.assertTrue(oldInvoiceLine.getQuantity() == quantity);
        Assert.assertTrue(oldInvoiceLine.getTotalPrice() == totalPrice);
        Assert.assertTrue(oldInvoiceLine.getBtwCode().equals(btwCode));
        Assert.assertTrue(oldInvoiceLine.getUnit().equals(unit));
    }
}
