package bifi.importmodel;

public class OldInvoiceLine {
    private String productName;
    private double quantity;
    private double totalPrice;
    private String btwCode;
    private String unit;

    public OldInvoiceLine(String productName, double quantity, double totalPriceExBtw, String btwCode, String unit) {
        this.productName = productName;
        this.quantity = quantity;
        this.totalPrice = totalPriceExBtw;
        this.btwCode = btwCode;
        this.unit = unit;
    }

    public String getProductName() {
        return productName;
    }

    public double getQuantity() {
        return quantity;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public String getBtwCode() {
        return btwCode;
    }

    public String getUnit() {
        return unit;
    }
}
