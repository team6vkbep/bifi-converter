package bifi.importmodel;

public class OldPerson {
    private String firstName;
    private String insertion;
    private String lastName;
    private String gender;

    public OldPerson(String firstName, String insertion, String lastName, String gender) {
        this.firstName = firstName;
        this.insertion = insertion;
        this.lastName = lastName;
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getInsertion() {
        return insertion;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCompleteName(){
        return this.firstName + this.insertion + this.lastName;
    }

    public String getSalutation(){
        switch (this.gender) {
            case "m":  return "Dhr.";
            case "f":  return "Mvr.";
            default: return "Dhr.";
        }
    }
}
