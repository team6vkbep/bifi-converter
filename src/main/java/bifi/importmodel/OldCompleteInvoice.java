package bifi.importmodel;

import java.util.List;

public class OldCompleteInvoice {
    private List<OldInvoice> invoices;

    public OldCompleteInvoice(List<OldInvoice> invoices) {
        this.invoices = invoices;
    }

    public List<OldInvoice> getInvoices() {
        return invoices;
    }
}
