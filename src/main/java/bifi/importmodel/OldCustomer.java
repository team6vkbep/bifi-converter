package bifi.importmodel;

public class OldCustomer {
    private String companyName;
    private String vat;
    private String iban;
    private OldAddress oldAddress;

    public OldCustomer(String companyName, String vat, String iban, OldAddress oldAddress) {
        this.companyName = companyName;
        this.vat = vat;
        this.iban = iban;
        this.oldAddress = oldAddress;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getVat() {
        return vat;
    }

    public String getIban() {
        return iban;
    }

    public OldAddress getAddress() {
        return oldAddress;
    }
}
