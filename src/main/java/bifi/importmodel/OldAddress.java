package bifi.importmodel;

public class OldAddress {
    private String street;
    private String addressType;
    private String houseNumber;
    private String postalCode;
    private String city;
    private String bic;

    public OldAddress(String street, String addressType, String houseNumber, String postalCode, String city, String bic) {
        this.street = street;
        this.addressType = addressType;
        this.houseNumber = houseNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.bic = bic;
    }

    public String getStreet() {
        return street;
    }

    public String getAddressType() {
        return addressType;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    public String getBic() {
        return bic;
    }
}
