package bifi.importmodel;

import java.util.List;

public class OldInvoice {
    private String date;
    private String invoiceNumber;
    private OldCustomer oldCustomer; // Company
    private OldPerson oldPerson; // Customer
    private List<OldInvoiceLine> oldInvoiceLines;

    public OldInvoice(String date, String invoiceNumber, OldCustomer oldCustomer, OldPerson oldPerson, List<OldInvoiceLine> oldInvoiceLines) {
        this.date = date;
        this.invoiceNumber = invoiceNumber;
        this.oldCustomer = oldCustomer;
        this.oldPerson = oldPerson;
        this.oldInvoiceLines = oldInvoiceLines;
    }

    public String getDate() {
        return date;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public OldCustomer getCustomer() {
        return oldCustomer;
    }

    public OldPerson getOldPerson() {
        return oldPerson;
    }

    public List<OldInvoiceLine> getInvoiceLines() {
        return oldInvoiceLines;
    }
}
