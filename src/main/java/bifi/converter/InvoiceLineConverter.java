package bifi.converter;

import bifi.exportmodel.InvoiceLine;
import bifi.importmodel.OldInvoice;
import bifi.importmodel.OldInvoiceLine;
import java.util.ArrayList;
import java.util.List;

public class InvoiceLineConverter {

    public List<InvoiceLine> createInvoiceLines(OldInvoice invoice){
        List<InvoiceLine> invoiceLinesExport = new ArrayList<>();

        for (OldInvoiceLine invoiceLine : invoice.getInvoiceLines()){
            String invoiceTime = invoice.getDate();
            String productDescription = invoiceLine.getProductName();
            double amount = invoiceLine.getQuantity();
            double priceWithoutBtw = invoiceLine.getTotalPrice();
            String btwType = invoiceLine.getBtwCode();
            String unit = invoiceLine.getUnit();

            invoiceLinesExport.add(new InvoiceLine(invoiceTime, productDescription, amount, priceWithoutBtw, btwType, unit));
        }
        return invoiceLinesExport;
    }
}
