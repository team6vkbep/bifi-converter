package bifi.converter;

import bifi.exportmodel.Invoice;
import bifi.importmodel.OldInvoice;

public class InvoiceConverter {

    public Invoice createInvoice(OldInvoice invoice){
        String invoiceDate = invoice.getDate(); // DDMMYY
        String invoiceNumber = invoice.getInvoiceNumber();

        return new Invoice(invoiceDate, invoiceNumber);
    }
}
