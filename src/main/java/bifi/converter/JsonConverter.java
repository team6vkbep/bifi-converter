package bifi.converter;

import bifi.exportmodel.CompleteInvoice;
import bifi.importmodel.OldCompleteInvoice;
import com.google.gson.*;

import java.util.logging.Level;
import java.util.logging.Logger;

public class JsonConverter {
	private static final Logger LOGGER = Logger.getLogger( ExportAddress.class.getName() );

	public CompleteInvoice convertJsonStringToCompleteInvoiceExport(String jsonString){
		CompleteInvoiceConverter completeInvoiceConverter = new CompleteInvoiceConverter();
		try{
			OldCompleteInvoice oldCompleteInvoice = convertJsonStringToCompleteInvoiceImport(jsonString);
			return completeInvoiceConverter.convertOldCompleteInvoiceToCompleteInvoice(oldCompleteInvoice);
		}
		catch (NullPointerException e){
			LOGGER.log(Level.FINE, e.toString());
			return null;
		}
	}

	private OldCompleteInvoice convertJsonStringToCompleteInvoiceImport(String jsonString){
		Gson g = new Gson();
		try{
			return g.fromJson(jsonString, OldCompleteInvoice.class);
		}
		catch(Exception e) {
			LOGGER.log(Level.FINE, e.toString());
			return null;
		}
	}
}
