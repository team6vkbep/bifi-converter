package bifi.converter;

import bifi.exportmodel.Address;
import bifi.exportmodel.AddressTypeEnum;
import bifi.importmodel.OldAddress;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ExportAddress {
    private static final Logger LOGGER = Logger.getLogger( ExportAddress.class.getName() );
    private Address address;

    public ExportAddress(OldAddress oldAddress){
        createExportAddress(oldAddress);
    }

    private void createExportAddress(OldAddress oldAddress){
        String street = oldAddress.getStreet();
        AddressTypeEnum addressType = convertIntToAddressTypeEnum(oldAddress.getAddressType());
        String houseNumber = oldAddress.getHouseNumber();
        String postalCode = oldAddress.getPostalCode();
        String city = oldAddress.getCity();
        String bic = oldAddress.getBic();

        address = new Address(street, addressType, houseNumber, postalCode, city, bic);
    }

    public Address getAddressExport(){
        return this.address;
    }

    private AddressTypeEnum convertIntToAddressTypeEnum(String addressType){
        try{
            return AddressTypeEnum.valueOf(addressType);
        }
        catch(Exception e){
            LOGGER.log(Level.FINE, e.toString());
            return AddressTypeEnum.ADDRESS_TYPE_INVOICE;
        }
    }
}
