package bifi.converter;

import bifi.exportmodel.Company;
import bifi.exportmodel.Address;
import bifi.importmodel.OldAddress;
import bifi.importmodel.OldInvoice;

public class CompanyConverter {

    public Company createCompany(OldInvoice invoice){
        String name = invoice.getCustomer().getCompanyName();
        String btwCode = invoice.getCustomer().getVat();
        String iban = invoice.getCustomer().getIban();
        Address address = getCompanyAddress(invoice.getCustomer().getAddress());

        return new Company(name, btwCode, iban, address);
    }

    private Address getCompanyAddress(OldAddress oldAddress){
        ExportAddress exportAddress = new ExportAddress(oldAddress);
        return exportAddress.getAddressExport();
    }
}
