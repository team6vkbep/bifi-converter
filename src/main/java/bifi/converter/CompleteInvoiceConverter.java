package bifi.converter;

import bifi.exportmodel.Company;
import bifi.exportmodel.CompleteInvoice;
import bifi.exportmodel.Invoice;
import bifi.importmodel.OldCompleteInvoice;
import bifi.importmodel.OldInvoice;
import bifi.exportmodel.Customer;

public class CompleteInvoiceConverter {
    private CompleteInvoice completeInvoice;
    private CompanyConverter companyConverter;
    private CustomerConverter customerConverter;
    private InvoiceConverter invoiceConverter;
    private InvoiceLineConverter invoiceLineConverter;

    public CompleteInvoice convertOldCompleteInvoiceToCompleteInvoice(OldCompleteInvoice oldCompleteInvoice){
        completeInvoice = new CompleteInvoice();
        companyConverter = new CompanyConverter();
        customerConverter = new CustomerConverter();
        invoiceConverter = new InvoiceConverter();
        invoiceLineConverter = new InvoiceLineConverter();

        for (OldInvoice invoice : oldCompleteInvoice.getInvoices()){
            addCompanyToInvoiceExport(invoice);
            Company company = completeInvoice.findCompany(invoice.getCustomer().getCompanyName());
            addCustomerToCompany(invoice,company);
            Customer customer = company.findCustomer(invoice.getOldPerson().getCompleteName());
            addInvoiceToCustomer(invoice, customer);
            Invoice invoiceExport = customer.findInvoice(invoice.getInvoiceNumber());
            addInvoiceLinesToInvoice(invoice, invoiceExport);
        }
        return completeInvoice;
    }

    private void addCompanyToInvoiceExport(OldInvoice invoice){
        if (completeInvoice.checkIfCompanyIsNew(invoice.getCustomer().getCompanyName())){
            completeInvoice.addCompany(companyConverter.createCompany(invoice));
        }
    }

    private void addCustomerToCompany(OldInvoice invoice, Company company){

        if (completeInvoice.checkIfCustomerIsNew(invoice.getOldPerson().getCompleteName())){
           company.addCustomer(customerConverter.createCustomer(invoice));
        }
    }

    private void addInvoiceToCustomer(OldInvoice invoice, Customer customer){
         customer.addInvoice(invoiceConverter.createInvoice(invoice));
    }

    private void addInvoiceLinesToInvoice(OldInvoice oldInvoice, Invoice invoice){
        invoice.addInvoiceLines(invoiceLineConverter.createInvoiceLines(oldInvoice));
    }
}
