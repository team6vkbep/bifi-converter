package bifi.converter;

import bifi.exportmodel.Address;
import bifi.exportmodel.Customer;
import bifi.importmodel.OldAddress;
import bifi.importmodel.OldInvoice;

public class CustomerConverter {

    public Customer createCustomer(OldInvoice invoice){
        String salutation = invoice.getOldPerson().getSalutation();
        String firstName = invoice.getOldPerson().getFirstName();
        String insertion = invoice.getOldPerson().getInsertion();
        String lastName = invoice.getOldPerson().getLastName();
        String btwCode = invoice.getCustomer().getVat();
        String iban = invoice.getCustomer().getIban();
        Address address = getCustomerAddress(invoice.getCustomer().getAddress());

        return new Customer(salutation, firstName,  insertion,  lastName,  btwCode,  iban,  address);
    }

    private Address getCustomerAddress(OldAddress oldAddress){
        ExportAddress exportAddress = new ExportAddress(oldAddress);
        return exportAddress.getAddressExport();
    }
}
