package bifi.exportmodel;

import java.util.ArrayList;
import java.util.List;

public class Invoice {
    private String invoiceDate; // DDMMYY
    private String invoiceNumber;
    private List<InvoiceLine> invoiceLines = new ArrayList<>();

    public Invoice(String invoiceDate, String invoiceNumber) {
        this.invoiceDate = invoiceDate;
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public List<InvoiceLine> getInvoiceLines() {
        return invoiceLines;
    }

    public void addInvoiceLines(List<InvoiceLine> invoiceLineslist){
        for (InvoiceLine invoiceLine : invoiceLineslist)
        {
            invoiceLines.add(invoiceLine);
        }
    }
}
