package bifi.exportmodel;

import java.util.ArrayList;
import java.util.List;

public class Customer {
        private String salutation;
        private String firstName;
        private String insertion;
        private String lastName;
        private String btwCode;
        private String iban;
        private Address address;
        private List<Invoice> invoices = new ArrayList<>();

        public Customer(String salutation, String firstName, String insertion, String lastName, String btwCode, String iban, Address address) {
                this.salutation = salutation;
                this.firstName = firstName;
                this.insertion = insertion;
                this.lastName = lastName;
                this.btwCode = btwCode;
                this.iban = iban;
                this.address = address;
        }

        public String getSalutation() {
                return salutation;
        }

        public String getFirstName() {
                return firstName;
        }

        public String getInsertion() {
                return insertion;
        }

        public String getLastName() {
                return lastName;
        }

        public String getBtwCode() {
                return btwCode;
        }

        public String getIban() {
                return iban;
        }

        public Address getAddress() {
                return address;
        }

        public List<Invoice> getInvoices() {
                return invoices;
        }

        public String getCompleteName(){
                return this.firstName + this.insertion + this.lastName;
        }

        public void addInvoice(Invoice invoice){
                invoices.add(invoice);
        }

        public Invoice findInvoice(String invoiceNumber){
                for (Invoice invoice : getInvoices()){
                        if (invoice.getInvoiceNumber().equals(invoiceNumber)){
                                return invoice;
                        }
                }
                return null;
        }
}
