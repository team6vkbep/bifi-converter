package bifi.exportmodel;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private String name;
    private String btwCode;
    private String iban;
    private Address address;
    private List<Customer> customers = new ArrayList<>();

    public Company(String name, String btwCode, String iban, Address address) {
        this.name = name;
        this.btwCode = btwCode;
        this.iban = iban;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getBtwNumber() {
        return btwCode;
    }

    public String getIban() {
        return iban;
    }

    public Address getAddress() {
        return address;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void addCustomer(Customer customer){
        customers.add(customer);
    }

    public boolean checkIfCustomerIsNew(String completeName){
        if(customers != null){
        for (Customer customer : customers){
            if (customer.getCompleteName() == completeName){
                return false;
            }
        }
        }
        return true;
    }

    public Customer findCustomer(String completeName){
        for (Customer customer : customers){
            if (customer.getCompleteName().equals(completeName)){
                return customer;
            }
        }
        return null;
    }
}
