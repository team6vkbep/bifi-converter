package bifi.exportmodel;

public enum AddressTypeEnum {
    ADDRESS_TYPE_INVOICE{
        @Override
        public String toString() {
            return "i";
        }
    },
    ADDRESS_TYPE_MAILING{
        @Override
        public String toString() {
            return "m";
        }
    },
    ADDRESS_TYPE_DELIVERY{
        @Override
        public String toString() {
            return "d";
        }
    }
}
