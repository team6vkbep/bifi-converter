package bifi.exportmodel;

public class InvoiceLine {
    private String invoiceTime; // HHMM
    private String productDescription;
    private double amount;
    private double priceWithoutBtw;
    private String btwType;
    private String unit;

    public InvoiceLine(String invoiceTime, String productDescription, double amount, double priceWithoutBtw, String btwType, String unit) {
        this.invoiceTime = invoiceTime;
        this.productDescription = productDescription;
        this.amount = amount;
        this.priceWithoutBtw = priceWithoutBtw;
        this.btwType = btwType;
        this.unit = unit;
    }

    public String getInvoiceTime() {
        return invoiceTime;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public double getAmount() {
        return amount;
    }

    public double getPriceWithoutBtw() {
        return priceWithoutBtw;
    }

    public String getBtwType() {
        return btwType;
    }

    public String getUnit() {
        return unit;
    }
}
