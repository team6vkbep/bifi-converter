package bifi.exportmodel;

import java.util.ArrayList;
import java.util.List;

public class CompleteInvoice {
    private List<Company> companies = new ArrayList<>();

    public boolean checkIfCompanyIsNew(String companyName){
        for (Company company : companies){
            if (company.getName() == companyName){
                return false;
            }
        }
        return true;
    }


    public Company findCompany(String companyName){
        for (Company company : companies){
            if (company.getName() == companyName){
                return company;
            }
        }
        return null;
    }
    public void addCompany(Company company){
        companies.add(company);
    }

    public boolean checkIfCustomerIsNew(String completeName){
        for (Company company : companies){
                if (!company.checkIfCustomerIsNew(completeName)){
                    return false;
                }
        }
        return true;
    }

    public   List<Company> getCompanies(){
        return  companies;
    }
}
